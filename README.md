# WireGuard Configulation Tool

## Required

- Installed WireGuard
- Installed qrencode

## Usage

### Initialization

Fill in settings.conf

```bash
cp example.settings.conf settings.conf
```

```settings.conf
SERVER_ADDRESS="x.x.x.x" # Public IP addres of your WireGuard server
SERVER_PORT="51820" # Server Port (default: 51820)
REAL_NIC="eth0" # Name of network adaptor (Check in "ip a" command)
VIRTUAL_NIC="wg0" # Name of virtual network adaptor
VPN_ADDRESS="10.101.0.0" # WireGuard inbound network address
DNS="1.1.1.1"
```

```bash
./initialization.sh
```

### Add client config

```bash
./genclient.sh [Client Name] [Host Number]
```