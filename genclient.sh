#!/bin/bash
# Usage: genclient ${CLIENT_NAME} ${Host part of IP address}

if [ "$#" != "2" ]; then
    echo "You must provide two args."
fi

source ./settings.conf

CLIENT_NAME=$1
HOST=$2
echo ${VPN_ADDRESS}
VPN_CLIENT_ADDRESS=$(echo ${VPN_ADDRESS} | sed -r "s/[0-9]{1,3}/${HOST}/4")
SERVER_PUBLICKEY=$(cat keys/server_public.key)
PRESHARED_KEY=$(cat keys/preshared.key)

# Generate Pair of Keys
echo Generating pair of keys...
CLIENT_PRIVATEKEY=$(wg genkey | tee ./keys/${CLIENT_NAME}_private.key)
CLIENT_PUBLICKEY=$(wg pubkey < ./keys/${CLIENT_NAME}_private.key | tee ./keys/${CLIENT_NAME}_public.key)
chmod 600 ./keys/${CLIENT_NAME}*.key

# Generate Client config file
echo Generating client config file...
while read line || [ -n "${line}" ]
do
    echo $(eval echo "''${line}")
done < ./template/client.conf > ./clients/${CLIENT_NAME}.conf

# Generate QR code
echo Generating QR code...
qrencode -o ./clients/qr/${CLIENT_NAME}.png -d 350 -r ./clients/${CLIENT_NAME}.conf

# Generate Server config
echo Generating server config...
while read line || [ -n "${line}" ]
do
    echo $(eval echo "''${line}")
done < ./template/server.conf >> ./conf/wg0.conf

if ${AUTO_APPLY_SERVER_CONFIG} ; then
    sudo cp ./conf/wg0.conf /etc/wireguard/
fi

# Restart WireGuard
if ${AUTO_RESTART} ; then
    echo Restarting WireGuard...
    sudo wg-quick down wg0 > /dev/null
    sudo wg-quick up wg0 > /dev/null
fi