#!/bin/bash

source ./settings.conf

# Generate Keys

wg genkey > keys/server_private.key
wg pubkey < keys/server_private.key > keys/server_public.key
wg genkey > keys/preshared.key
chmod 600 keys/server_private.key keys/server_public.key keys/preshared.key

# Generate Initial Setting

VPN_SERVER_ADDRESS=$(echo ${VPN_ADDRESS} | sed -r "s/[0-9]{1,3}/1/4")
SERVER_PRIVATE_KEY=$(cat keys/server_private.key)

cat << EOS > ./conf/${VIRTUAL_NIC}.conf
[Interface]
Address = ${VPN_SERVER_ADDRESS}/24
PostUp = iptables -A FORWARD -i ${VIRTUAL_NIC} -j ACCEPT; iptables -t nat -A POSTROUTING -o ${REAL_NIC} -j MASQUERADE; ufw route allow in on ${VIRTUAL_NIC} out on ${REAL_NIC}; ufw route allow in on ${REAL_NIC} out on ${VIRTUAL_NIC}; ufw allow proto udp from any to any port 51820
PostDown = iptables -D FORWARD -i ${VIRTUAL_NIC} -j ACCEPT; iptables -t nat -D POSTROUTING -o ${REAL_NIC} -j MASQUERADE; ufw route delete allow in on ${VIRTUAL_NIC} out on ${REAL_NIC}; ufw route delete allow in on ${REAL_NIC} out on ${VIRTUAL_NIC}; ufw delete allow proto udp from any to any port 51820
ListenPort = ${SERVER_PORT}
PrivateKey = ${SERVER_PRIVATE_KEY}
EOS